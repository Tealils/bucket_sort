require 'benchmark'
include Benchmark

class SortTest
	attr_accessor :test, :std

	def initialize(num,type="R")
		@type = type
		@std = Array.new(0)
		if type.eql?('R')
			num.times do |i|
				@std[i] = (num * 100 * rand).round
			end
		else
			num.times do |i|
				@std[i] = num - i
			end
		end
	end
	
	def reset
		@test = @std.dup
	end

	SortTest::NUMBUCKETS = 16
	def bucketSort(a = @test)
		return a.sort if a.length < NUMBUCKETS
		minkey = a.min; maxkey = a.max
		return a if minkey.eql?(maxkey)
				
		buckets = fillbuckets(a,minkey,maxkey)
		bList = Array.new
		buckets.each do |bucket|
			bList << bucketSort(bucket)
		end
		bList.flatten!
		if bList.length == @test.length
			@test = bList
		end
		return bList
	end

	def fillbuckets(a,minkey,maxkey)
		keysize = (NUMBUCKETS - 1.0) / (1+ maxkey - minkey)

		buckets = Array.new
		NUMBUCKETS.times do |i|
			buckets[i] = Array.new
		end
			
		a.each do |x|
			buckets[((x-minkey) * keysize).floor] << x
		end
		return buckets
	end
	
	def insertionSort
		1.upto((@test.length) - 1) do |i|
			i.downto(1) do |j|
				if @test[j-1] > @test[j]
					@test[j-1], @test[j] = @test[j], @test[j-1]
				else
					break
				end
			end
		end
	end
	
	def partition(lo,hi)
		mid = ((lo + hi)/2).floor
		if @test[mid] <= @test[hi]
		   @test[mid], @test[hi] = @test[hi], @test[mid]
		end
		pivot = hi
		j = hi - 1; i = lo

		while i < j 
			while @test[i] < @test[pivot] do i += 1 end
			while @test[j] > @test[pivot] do j -= 1 end
			
			if i < j
				@test[i], @test[j] = @test[j], @test[i]
				i += 1
			end
		end

		@test[pivot], @test[i] = @test[i], @test[pivot]
		return i
	end

end

repetition = 100
["RevSeq","Random"].each do |typ|
4.times do |iteration|
[100,1000].each do |siz|
t = SortTest.new(siz,type=typ)

bmbm(25) do |test|
	STDERR.puts "%12s, %s, %5d, %2d" % ["0,Control",typ,siz,iteration] 
	test.report("%12s, %s, %5d, %2d," % ["0,Control",typ,siz,iteration]) do
		repetition.times do
			t.reset
			# puts t.test.inspect
			# puts t.test.inspect
		end
	end

	STDERR.puts "%12s, %s, %5d, %2d" % ["1,Insertion",typ,siz,iteration]
	test.report("%s,%s, %5d, %2d," % ["1,Insertion",typ,siz,iteration]) do
		repetition.times do
			t.reset
			# puts t.test.inspect
			t.insertionSort
			# puts t.test.inspect
		end
	end

	STDERR.puts "%12s, %s, %5d, %2d" % ["2,Bucket",typ,siz,iteration]
	test.report("%s,%s, %5d, %2d," % ["2,Bucket",typ,siz,iteration]) do
		repetition.times do
			t.reset
			# puts t.test.inspect
			t.bucketSort
			# puts t.test.inspect
		end
	end

end

end
end
end